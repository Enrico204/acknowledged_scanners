# ncsc
# UK National Cyber Security Centre Scanning Capability
# Source: https://www.ncsc.gov.uk/information/ncsc-scanning-information
# 
# Range: The NCSC scans should only be affecting the UK. 
#
18.171.7.246
35.177.10.231
